import { Switch, Route } from 'react-router-dom';

import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import Enterprise from '../pages/Enterprise';
import ProvideAuth from './ProvideAuth';
import PrivateRoute from './PrivateRoute';

function Routes() {
  return (
    <Switch>
      <ProvideAuth>
        <Route path="/" exact component={Login} />
        <PrivateRoute path="/dashboard">
          <Dashboard />
        </PrivateRoute>
        <PrivateRoute path="/enterprise">
          <Enterprise />
        </PrivateRoute>
      </ProvideAuth>
    </Switch>
  );
}

export default Routes;
