import { useState } from 'react';
import Authentication from '../repositories/Authentication';

function useProvideAuth() {
  const [user, setUser] = useState(null);

  const signin = (email, password) =>
    Authentication.signIn({ email, password }).then(response => {
      const data = {
        accessToken: response.headers['access-token'],
        client: response.headers.client,
        uid: response.headers.uid,
      };

      setUser(data);
      return data;
    });

  return {
    user,
    signin,
  };
}

export default useProvideAuth;
