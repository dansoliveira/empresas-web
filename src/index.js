import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from './App';
import LoadingFullscreen from './components/LoadingFullscreen';

import rootReducer from './reducers';

const store = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
      <LoadingFullscreen />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
