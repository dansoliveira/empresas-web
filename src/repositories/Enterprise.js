import api from '../services/api';

function index(data) {
  const { filter, user } = data;
  return api.get('/enterprises', {
    params: filter,
    headers: {
      'access-token': user.accessToken,
      client: user.client,
      uid: user.uid,
    },
  });
}

function show(data) {
  const { id } = data;

  return api.get(`/enterprises/${id}`);
}

export default { index, show };
