import api from '../services/api';

function signIn(data) {
  return api.post('users/auth/sign_in', data);
}

export default { signIn };
