import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --white: #d8d8d8;
    --white-two: #ffffff;
    --dark-indigo: #1a0e49;
    --warm-grey: #8d8c8c;
    --greeny-blue: #57bbbc;
    --medium-pink: #ee4c77;
    --night-blue: #0d0430;
    --charcoal-grey: #383743;
    --charcoal-grey-two: #403e4d;
    --greyish: #b5b4b4;
    --red: #ff0f44;
    --greyish-blue: #748383;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    background-color: #ebe9d7;
    font: 1.125rem Roboto, sans-serif;
  }
`;
