const initialState = {};

export default function enterpriseReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_ENTERPRISE':
      return {
        ...action.enterprise,
      };
    default:
      return state;
  }
}
