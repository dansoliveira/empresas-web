const initialState = {
  showLoadingFullscreen: false,
};

export default function generalReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_SHOW_LOADING_FULLSCREEN':
      return {
        ...state,
        showLoadingFullscreen: action.showLoadingFullscreen,
      };
    default:
      return state;
  }
}
