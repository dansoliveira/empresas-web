import { combineReducers } from 'redux';
import enterpriseReducer from './enterprise';
import generalReducer from './general';

const rootReducer = combineReducers({
  enterprise: enterpriseReducer,
  general: generalReducer,
});

export default rootReducer;
