import styled, { css } from 'styled-components';

import { LogoWhite } from '../Logo';

import SearchIconImage from '../../assets/icons/ic-search.svg';
import CloseIconImage from '../../assets/icons/ic-close.svg';

export const Header = styled.nav`
  height: 10vh;
  display: grid;
  ${props =>
    props.$searchMode
      ? css`
          grid-template-columns: 1fr;
          align-items: center;
        `
      : css`
          grid-template-columns: 1fr 4rem;
          place-items: center;
        `}
  justify-content: center;
  background-image: linear-gradient(
    179deg,
    var(--medium-pink) 30%,
    var(--night-blue) 200%
  );
`;

export const LogoNav = styled(LogoWhite)`
  width: 14.64rem;
  height: 3.563rem;
  object-fit: contain;
`;

export const SearchIcon = styled.img.attrs(() => ({
  src: SearchIconImage,
}))`
  width: 100%;
  height: auto;
  object-fit: contain;
`;

export const SearchIconWrapper = styled.button`
  width: 100%;
  border: none;
  background: none;
  cursor: pointer;
`;

export const CloseIcon = styled.img.attrs(() => ({
  src: CloseIconImage,
}))`
  width: 100%;
  height: auto;
  object-fit: contain;
`;

export const SearchFieldWrapper = styled.div`
  margin: 0 2.625rem;
`;
