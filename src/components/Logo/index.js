import styled from 'styled-components';

import LogoPinkImage from '../../assets/images/logo-home.png';
import LogoPinkImage2x from '../../assets/images/logo-home@2x.png';
import LogoPinkImage3x from '../../assets/images/logo-home@3x.png';

import LogoWhiteImage from '../../assets/images/logo-nav.png';
import LogoWhiteImage2x from '../../assets/images/logo-nav@2x.png';
import LogoWhiteImage3x from '../../assets/images/logo-nav@3x.png';

const Logo = styled.img`
  width: ${props => props.$width || '100%'};
  height: ${props => props.$height || '100%'};
`;

export const LogoPink = styled(Logo).attrs(() => ({
  src: LogoPinkImage3x,
  srcset: `
    ${LogoPinkImage},
    ${LogoPinkImage2x} 2x,
    ${LogoPinkImage3x} 3x
  `,
}))``;

export const LogoWhite = styled(Logo).attrs(() => ({
  src: LogoWhiteImage3x,
  srcset: `
    ${LogoWhiteImage},
    ${LogoWhiteImage2x} 2x,
    ${LogoWhiteImage3x} 3x
  `,
}))``;
