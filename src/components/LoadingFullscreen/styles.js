import styled from 'styled-components';

export const Background = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  backdrop-filter: blur(1px);
  background-color: rgba(255, 255, 255, 0.6);
  display: ${props => (props.show ? 'flex' : 'none')};
  justify-content: center;
  place-items: center;
`;

export const Spinner = styled.div`
  border: 8px solid rgba(0, 0, 0, 0.1);
  border-left-color: var(--greeny-blue);
  border-radius: 50%;
  width: 5rem;
  height: 5rem;
  animation: spin 1.2s linear infinite;

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }
`;
