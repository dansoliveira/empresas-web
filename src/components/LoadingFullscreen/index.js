import { useSelector } from 'react-redux';
import { Background, Spinner } from './styles';

function LoadingFullscreen() {
  const showLoadingFullscreen = useSelector(
    state => state.general.showLoadingFullscreen,
  );

  return (
    <Background show={showLoadingFullscreen}>
      <Spinner />
    </Background>
  );
}

export default LoadingFullscreen;
