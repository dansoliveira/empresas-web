import styled from 'styled-components';

export const TextStyle5 = styled.p`
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: var(--white-two);
`;

export const TextStyle4 = styled.p`
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #991237;
`;

export const TextStyle3 = styled.p`
  font-family: Roboto;
  font-size: 2rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.22;
  letter-spacing: -0.45px;
  text-align: center;
  color: var(--charcoal-grey);
`;

export const TextStyle6 = styled.p`
  font-family: Roboto;
  font-size: 1.875rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: var(--dark-indigo);
`;

export const TextStyle11 = styled.p`
  font-family: Roboto;
  font-size: 1.6rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -1.2px;
  text-align: center;
  color: var(--charcoal-grey);
`;

export const TextStyle10 = styled.p`
  font-family: Roboto;
  font-size: 1.5rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: var(--charcoal-grey);
`;

export const TextStyle7 = styled.p`
  font-family: Roboto;
  font-size: 1.5rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: var(--warm-grey);
`;

export const TextStyle9 = styled.p`
  font-family: SourceSansPro;
  font-size: 1.5rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: -0.38px;
  text-align: left;
  color: var(--warm-grey);
`;

export const TextStyle8 = styled.p`
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: var(--warm-grey);
`;

export const TextStyle2 = styled.p`
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.25px;
  text-align: left;
  color: var(--charcoal-grey);
`;

export const TextStyle = styled.p`
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.44;
  letter-spacing: -0.25px;
  text-align: center;
  color: var(--charcoal-grey);
`;

export const ErrorText = styled.p`
  font-family: Roboto;
  font-size: 0.76rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
  color: #ff0f44;
`;
