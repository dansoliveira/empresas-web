import styled from 'styled-components';

export const Card = styled.div`
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  gap: 2.4rem;
  padding: 1.6rem;
  border-radius: 4.7px;
  background-color: var(--white-two);
`;

export const Image = styled.img`
  flex: 1;
  max-width: 18rem;
  width: 100%;
  height: 10rem;
  object-fit: contain;
`;

export const Informations = styled.div`
  width: auto;
  display: flex;
  flex-direction: column;
`;

export const EnterpriseWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 2.75rem;
  gap: 3rem;
`;
