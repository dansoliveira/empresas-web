import styled from 'styled-components';

const Button = styled.button`
  cursor: pointer;
  width: 20.25rem;
  height: 3.3rem;
  border: none;
  border-radius: 4px;
  background-color: var(--greeny-blue);
  font-family: Roboto;
  font-size: 1.208rem;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: var(--white-two);

  &:disabled {
    background-color: var(--greyish-blue);
    opacity: 0.56;
  }
`;

export default Button;
