import { AddOn, Input, InputGroup, ErrorIcon } from './styles';

function Field({
  addOnLeft,
  addOnLeftOnClick,
  addOnRight,
  addOnRightOnClick,
  error,
  inputRef,
  isSecondary,
  ...otherProps
}) {
  return (
    <InputGroup>
      {addOnLeft && (
        <AddOn
          $isSecondary={isSecondary}
          $error={error}
          as={addOnLeftOnClick ? 'button' : 'div'}
          onClick={addOnLeftOnClick}
        >
          {addOnLeft}
        </AddOn>
      )}
      <Input
        $isSecondary={isSecondary}
        $error={error}
        ref={inputRef}
        {...otherProps}
      />
      <AddOn
        $isSecondary={isSecondary}
        $error={error}
        as={addOnRightOnClick ? 'button' : 'div'}
        onClick={addOnRightOnClick}
      >
        {error && <ErrorIcon $error={error}>!</ErrorIcon>}
        {!error && addOnRight}
      </AddOn>
    </InputGroup>
  );
}
export default Field;
