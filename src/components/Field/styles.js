import styled, { css } from 'styled-components';

const getInputBorderColor = props => {
  if (props.$error) return '--red';

  if (props.$isSecondary) return '--white-two';

  return '--charcoal-grey';
};

export const InputGroup = styled.div`
  display: flex;
  width: 100%;
`;

export const Input = styled.input`
  background: none;
  width: 100%;
  border: solid 0.7px var(${props => getInputBorderColor(props)});
  color: var(
    ${props => (props.$isSecondary ? '--white-two' : '--charcoal-grey-two')}
  );
  border-top: none;
  border-left: none;
  border-right: none;
`;

export const AddOn = styled.div`
  ${props =>
    props.as === 'button' &&
    css`
      cursor: pointer;
      background: none;
    `}
  display: flex;
  justify-content: center;
  align-items: center;
  width: 2rem;
  height: 2rem;
  border: solid 0.7px var(${props => getInputBorderColor(props)});
  border-top: none;
  border-left: none;
  border-right: none;
`;

export const ErrorIcon = styled.div`
  display: ${props => (props.$error ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
  width: 1.438rem;
  height: 1.5rem;
  margin: 0 0 0.063rem;
  background-color: var(--red);
  border-radius: 50%;
  font-family: Poppins;
  font-size: 1rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--white-two);
`;
