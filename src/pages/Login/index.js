import { useForm } from 'react-hook-form';

import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { ReactComponent as EmailIcon } from '../../assets/icons/ic-email.svg';
import { ReactComponent as PadlockIcon } from '../../assets/icons/ic-cadeado.svg';

import { LogoPink as Logo } from '../../components/Logo';
import Field from '../../components/Field';
import Button from '../../components/Button';
import {
  ContentWrapper,
  Form,
  Title,
  Description,
  InputWrapper,
  ErrorTextWrapper,
  EyeCharcoalGrey,
  EyeFillCharcoalGrey,
} from './styles';
import { ErrorText } from '../../components/Text';
import { useAuth } from '../../routes/useAuth';

function Login() {
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [errorApiCall, setErrorApiCall] = useState(false);
  const { handleSubmit, register } = useForm();
  const auth = useAuth();
  const history = useHistory();

  function handleLogin(formData) {
    const { email, password } = formData;

    dispatch({
      type: 'SET_SHOW_LOADING_FULLSCREEN',
      showLoadingFullscreen: true,
    });

    auth
      .signin(email, password)
      .then(() => {
        dispatch({
          type: 'SET_SHOW_LOADING_FULLSCREEN',
          showLoadingFullscreen: false,
        });
        history.push('/dashboard');
      })
      .catch(() => {
        dispatch({
          type: 'SET_SHOW_LOADING_FULLSCREEN',
          showLoadingFullscreen: false,
        });
        setErrorApiCall(true);
      });
  }

  useEffect(() => {
    if (auth.user) {
      history.push('/dashboard');
    }
  });

  return (
    <ContentWrapper>
      <Logo $width="18.438rem" $height="4.5rem" />
      <Title>BEM-VINDO AO EMPRESAS</Title>
      <Description>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </Description>
      <Form id="formLogin" onSubmit={handleSubmit(handleLogin)}>
        <InputWrapper>
          <Field
            addOnLeft={<EmailIcon />}
            name="email"
            placeholder="E-mail"
            inputRef={register({
              required: true,
            })}
            error={errorApiCall}
            onChange={() => {
              if (errorApiCall) {
                setErrorApiCall(false);
              }
            }}
          />
          <Field
            addOnLeft={<PadlockIcon />}
            addOnRight={
              showPassword ? <EyeFillCharcoalGrey /> : <EyeCharcoalGrey />
            }
            addOnRightOnClick={event => {
              event.preventDefault();
              setShowPassword(oldValue => !oldValue);
            }}
            name="password"
            type={showPassword ? 'text' : 'password'}
            placeholder="Senha"
            inputRef={register({
              required: true,
            })}
            error={errorApiCall}
            onChange={() => {
              if (errorApiCall) {
                setErrorApiCall(false);
              }
            }}
          />
        </InputWrapper>
        <ErrorTextWrapper>
          <ErrorText hidden={!errorApiCall}>
            Credenciais informadas são inválidas, tente novamente.
          </ErrorText>
        </ErrorTextWrapper>
        <Button type="submit" form="formLogin" disabled={errorApiCall}>
          ENTRAR
        </Button>
      </Form>
    </ContentWrapper>
  );
}

export default Login;
