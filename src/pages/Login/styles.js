import styled from 'styled-components';

import { ReactComponent as EyeIcon } from '../../assets/icons/ic-eye.svg';
import { ReactComponent as EyeFillIcon } from '../../assets/icons/ic-eye-fill.svg';

import { TextStyle, TextStyle10 } from '../../components/Text';

export const Form = styled.form`
  width: 21rem;
  display: flex;
  flex-direction: column;
  place-items: center;
`;

export const InputWrapper = styled.div`
  margin-top: 2.8rem;
  margin-bottom: 0.8rem;
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 2rem;
`;

export const ErrorTextWrapper = styled.div`
  height: 1.4rem;
  width: 100%;
  margin-bottom: 0.6rem;
`;

export const ContentWrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  place-items: center;
  justify-content: center;
`;

export const Title = styled(TextStyle10).attrs(() => ({
  as: 'div',
}))`
  margin-top: 4.188rem;
  width: 11rem;
  height: 4rem;
`;

export const Description = styled(TextStyle).attrs(() => ({
  as: 'div',
}))`
  margin-top: 1.281rem;
  width: 22.359rem;
  height: 3.25rem;
`;

export const EyeCharcoalGrey = styled(EyeIcon)`
  path {
    fill: var(--charcoal-grey);
  }
`;

export const EyeFillCharcoalGrey = styled(EyeFillIcon)`
  path {
    fill: var(--charcoal-grey);
  }
`;
