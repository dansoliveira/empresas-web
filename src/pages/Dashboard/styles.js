import styled from 'styled-components';
import { TextStyle8 } from '../../components/Text';

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const NoContent = styled(Content)`
  height: 90vh;
`;

export const FirstTimeText = styled(TextStyle8)`
  font-size: 2rem;
  color: var(--charcoal-grey);
  text-align: center;
`;

export const NoContentText = styled(TextStyle8)`
  font-size: 2.125rem;
  color: var(--greyish);
  text-align: center;
  margin: 0 2rem;
`;

export const DashboardWrapper = styled.div``;
