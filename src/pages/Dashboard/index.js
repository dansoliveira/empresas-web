import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Field from '../../components/Field';
import {
  CloseIcon,
  Header,
  LogoNav,
  SearchIcon,
  SearchIconWrapper,
  SearchFieldWrapper,
} from '../../components/Navbar';
import {
  DashboardWrapper,
  Content,
  NoContentText,
  FirstTimeText,
  NoContent,
} from './styles';
import Enterprise from '../../repositories/Enterprise';
import { useAuth } from '../../routes/useAuth';
import EnterpriseCard from './EnterpriseCard';
import { EnterpriseWrapper } from '../../components/Enterprise';

function Dashboard() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { user } = useAuth();
  const [isOnSearchMode, setIsOnSearchMode] = useState(false);
  const [showNoDataMessage, setShowNoDataMessage] = useState(false);
  const [searchTimeout, setSearchTimeout] = useState(null);
  const [searchType, setSearchType] = useState('nome');
  const [searchValue, setSearchValue] = useState('');
  const [enterprises, setEnterprises] = useState([]);
  const noDataAfterSearch = isOnSearchMode && showNoDataMessage;
  const showNoContent =
    (isOnSearchMode && showNoDataMessage) || !isOnSearchMode;

  useEffect(() => {
    if (!user) {
      history.push('/');
    }
  });

  useEffect(() => {
    if (searchTimeout) clearTimeout(searchTimeout);

    if (!user) return;

    dispatch({
      type: 'SET_SHOW_LOADING_FULLSCREEN',
      showLoadingFullscreen: true,
    });

    const timeout = setTimeout(() => {
      const filter = searchType === 'nome' ? 'name' : 'enterprise_types';

      setShowNoDataMessage(false);

      Enterprise.index({
        filter: {
          [filter]: searchValue,
        },
        user,
      })
        .then(response => {
          dispatch({
            type: 'SET_SHOW_LOADING_FULLSCREEN',
            showLoadingFullscreen: false,
          });

          if (response.data.enterprises.length === 0) {
            setShowNoDataMessage(true);
          }
          setEnterprises(response.data.enterprises);
        })
        .catch(error => {
          dispatch({
            type: 'SET_SHOW_LOADING_FULLSCREEN',
            showLoadingFullscreen: false,
          });

          if (error.response.status === 401) {
            history.push('/');
          }
        });
    }, 500);

    setSearchTimeout(timeout);
  }, [searchValue]);

  return (
    <DashboardWrapper>
      <Header $searchMode={isOnSearchMode}>
        {!isOnSearchMode && (
          <>
            <LogoNav />
            <SearchIconWrapper
              onClick={event => {
                event.preventDefault();
                setIsOnSearchMode(true);
              }}
            >
              <SearchIcon />
            </SearchIconWrapper>
          </>
        )}
        {isOnSearchMode && (
          <SearchFieldWrapper>
            <Field
              isSecondary
              addOnLeft={<SearchIcon />}
              addOnLeftOnClick={event => {
                event.preventDefault();
              }}
              addOnRight={<CloseIcon />}
              addOnRightOnClick={event => {
                event.preventDefault();
                if (searchValue) {
                  setSearchValue('');
                } else {
                  setIsOnSearchMode(false);
                }
              }}
              name="search"
              placeholder="Pesquisar"
              onChange={({ target }) => {
                setSearchValue(target.value);
              }}
              value={searchValue}
            />
          </SearchFieldWrapper>
        )}
      </Header>
      {showNoContent && (
        <NoContent>
          {!isOnSearchMode && (
            <FirstTimeText>Clique na busca para iniciar.</FirstTimeText>
          )}
          {noDataAfterSearch && (
            <NoContentText>
              Nenhuma empresa foi encontrada para a busca realizada.
            </NoContentText>
          )}
        </NoContent>
      )}
      {isOnSearchMode && (
        <Content>
          <EnterpriseWrapper>
            {enterprises.length > 0 &&
              enterprises.map(enterprise => (
                <EnterpriseCard key={enterprise.id} enterprise={enterprise} />
              ))}
          </EnterpriseWrapper>
        </Content>
      )}
    </DashboardWrapper>
  );
}

export default Dashboard;
