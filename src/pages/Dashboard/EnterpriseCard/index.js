import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TextStyle8 } from '../../../components/Text';
import { Card, Image, Informations } from '../../../components/Enterprise';

function EnterpriseCard({ enterprise }) {
  const history = useHistory();
  const dispatch = useDispatch();

  return (
    <Card
      onClick={event => {
        event.preventDefault();
        dispatch({
          type: 'SET_ENTERPRISE',
          enterprise,
        });
        history.push('/enterprise');
      }}
    >
      <Image
        src={process.env.IMG_BASE_URL + enterprise.photo}
        alt="Foto da empresa"
        loading="lazy"
      />
      <Informations>
        <TextStyle8
          style={{
            fontSize: '1.875rem',
            fontWeight: 'bold',
            color: 'var(--dark-indigo)',
          }}
        >
          {enterprise.enterprise_name}
        </TextStyle8>
        <TextStyle8
          style={{
            fontSize: '1.5rem',
          }}
        >
          {enterprise.enterprise_type.enterprise_type_name}
        </TextStyle8>
        <TextStyle8>{enterprise.country}</TextStyle8>
      </Informations>
    </Card>
  );
}

export default EnterpriseCard;
