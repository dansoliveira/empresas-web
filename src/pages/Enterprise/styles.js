import styled from 'styled-components';

import { TextStyle8 } from '../../components/Text';

import ArrowLeftImage from '../../assets/icons/ic-arrow-left.svg';

export const ArrowLeft = styled.img.attrs(() => ({
  src: ArrowLeftImage,
}))`
  cursor: pointer;
  margin-left: 2rem;
  width: 100%;
  max-width: clamp(2rem, 2rem, 8rem);
  height: auto;
`;

export const Title = styled(TextStyle8)`
  text-transform: uppercase;
  font-size: 2.125rem;
  color: var(--white-two);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
