import { TextStyle8 } from '../../../components/Text';
import {
  Card,
  EnterpriseWrapper,
  Image,
  Informations,
} from '../../../components/Enterprise';

function DetailCard({ enterprise }) {
  return (
    <EnterpriseWrapper>
      <Card
        style={{
          cursor: 'auto',
        }}
      >
        <Image
          src={process.env.IMG_BASE_URL + enterprise.photo}
          alt="Foto da empresa"
          style={{
            width: '100%',
            maxWidth: 'none',
            height: '100%',
            maxHeight: '50vh',
            objectFit: 'contain',
          }}
        />
        <Informations>
          <TextStyle8
            style={{
              fontSize: '2.125rem',
              wordBreak: 'break-word',
            }}
          >
            {enterprise.description}
          </TextStyle8>
        </Informations>
      </Card>
    </EnterpriseWrapper>
  );
}

export default DetailCard;
