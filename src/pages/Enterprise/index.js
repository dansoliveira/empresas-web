import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Header } from '../../components/Navbar';
import DetailCard from './DetailCard';
import { ArrowLeft, Title } from './styles';

function Enterprise() {
  const history = useHistory();
  const enterprise = useSelector(state => state.enterprise);

  useEffect(() => {
    if (!enterprise.id) {
      history.goBack();
    }
  }, [enterprise]);

  return (
    <>
      <Header>
        <div
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'left',
            placeItems: 'center',
            gap: '2rem',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
          }}
        >
          <ArrowLeft
            onClick={event => {
              event.preventDefault();
              history.goBack();
            }}
          />
          <Title>{enterprise.enterprise_name}</Title>
        </div>
      </Header>
      <DetailCard enterprise={enterprise} />
    </>
  );
}

export default Enterprise;
