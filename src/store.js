import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers';

const middlewares = [thunk];
const configureStore = () =>
  createStore(
    combineReducers(
      {
        ...reducers,
      },
      applyMiddleware(...middlewares),
    ),
  );
export default configureStore;
